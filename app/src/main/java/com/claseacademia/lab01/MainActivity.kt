package com.claseacademia.lab01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         //find ID

          val nAge: EditText = findViewById(R.id.edtAge)
          val btnProcesa: Button = findViewById(R.id.btnProcesar)
          val sResultado:TextView =findViewById(R.id.edtResultado)

          btnProcesa.setOnClickListener {
              // get values
              val nEdad = nAge.text.toString()
              if (nEdad.toInt()>=18){
                  sResultado.text="Ud. es mayor de edad"
              } else {
                 sResultado.text="Ud. es menor de edad"
              }
          }
           }
}